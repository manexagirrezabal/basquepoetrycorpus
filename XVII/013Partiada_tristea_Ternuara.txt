Partitzean Ternuarat
untzia prest da belarat,
berga haut da arradan
denbora onaren paradan.

Xuri garbi, zeru gaina,
itsasoak eder maina,
haize ona belarako,
denbora ona partitzeko.

Untzitik artileria,
athean mandataria,
enbargadi marinela,
untziak dagien bela.

Partitzen da marinela,
bihotzean triste dela,
gomendatzen Jainkoari,
adios dio mainadari.

Adios aita, adios ama,
behar handiak narama,
neure burhaso zaharrak,
zuen hazteko beharrak.

Adios, neure emaztea,
esposa maite gaztea,
tristea dut partiada,
sekulakotz behar bada.

Alabainan goan behar naiz,
munduan biziko banaiz,
nik behar dut irabazi,
neurekin mainada hazi.

Bizi beharrez bizia,
benturatzen dut guzia,
bizi ustean hiltzera,
banoha hirriskatzera.

Itsasoak nau bizitzen,
hark berak nau ni izitzen,
bizi beharra emanen,
gero berak eramanen.

Adio, neure haur xumeak,
seme, alaba, maiteak,
galtzen baduzue aita,
ama alarguna maitha.

Adio, neure haurrideak,
adio, etxeko jendeak,
othoitz nitaz Jainkoari,
dudan amorez bidari.

Adi aitak semeari,
adi amak umeari,
adi bere emazteak,
nigarretan haur gazteak.

Neure senhar maite ona,
etxean behar gizona,
sekulakotz gal beldurrez,
adio darotzut nigarrez.

Jaunak dizula grazia,
salborik jende guzia,
arribatzeko bizirik,
biaian irabazirik.

Marinelen emazteak
hainitzak senhar gabeak,
goizean dena senardun,
egun berean alargun.

Marinelak mainadari,
mainadak marinelari,
adio erran eta pharti,
mainada uzten nigarti.

Burasoek seme ona,
esposak bere gizona,
haurrek ere bere aita,
ala galtzea min baita!

Marinelak bizia motz,
guti sortzen zahartzekoz,
gehienak gaztetikan
badohazi mundutikan.

Etxean beharrenean,
kalte dadiketenean,
orduban galtzen gizonak,
mainada, hartzaile onak.

Marinela phartitu da,
sekulakotz behar bada,
hainitz dohaz sekulakoz,
hil berria ethortzekoz.


Itsasoko perilak

Kontxatik doha untzia,
han konpainia guzia,
ternuarat badohazi,
zerbait nahiz irabazi.

Bide luze Ternuarat,
itsasoa zabal harat,
zubirik ez pasatzeko,
marinelen salbatzeko.

Itsasoaren ganean,
untzi tzar baten menean,
Marinela hirriskuan,
Herioaren eskuan.

Haize largoz badohazi,
zabalerat irabazi,
itsasoan barna urrun,
begiz ezin ikhus Larrun.

Han jotzen ditu kontrestak,
mendebal haize tenpestak,
denbora gaiztoa sartzen,
gabitau biak erortzen.

Zelu guzia isuri,
babazuza eta uri,
marinelak trenpatuak,
hotz handiak kharronduak.

Bela guziak harturik,
belaxoa anekaturik,
untzia badoha segiz,
itsasoa handiegiz.

Gau beltzean, ilhunbean,
untzia tormentapean,
aparailua desegin,
ilhunez deus ezin egin.

Elementak badarontsa,
itsasoak habarrotsa,
haize tenpesta uxia da,
itsasoan maskarada.

Furakanaren furia,
ifernuko iduria,
Satan beltzak darabila,
untzien galtzen dabila.

Marinelak harrituak,
tronpen jotzen unhatuak,
gabetu indar guziez,
etsitu bere biziez.

Uhainek untzia joka,
gorabehera saltoka,
eta branka pulunpaka,
kostaduba arrolaka.

Uhin baten bizkarrean,
bi uhinen hondarrean,
tirabira badabila,
untziak agertzen gila.

Bere mastak galdu ditu,
untzia motz da gelditu,
itsasoak hautsi lema,
trebes galtzerat darama.

Uhin hautsiak gainetik,
iragaten trebesetik,
marinelak erorika,
untzi askan igerika.

Koartera dio eraman,
kostadua barnat eman,
gain guzia arrasatu,
untzi zolera urratu.

Gaineko zubia hautsi,
ura tilaperat jautsi,
tilapean ura gora,
untzia doha hondora.

Marinelak biluziak,
luzatu nahiz biziak,
uhin pean igerika,
untzi puskei atxikika.

Ura non berma ez du,
ez aireak nondik lothu,
untziak ez zimendurik,
han ez da salbamendurik.

Marinelaren bentura,
itsasoan sepultura,
sekulako bere fina,
etxerako berriz mina.

Ternuako penak

Untzi batzu salbaturik,
itsasoa pasaturik,
arribatu Ternuarat,
behar duten portutarat.

Ternua da mortu hotza,
eremu triste arrotza,
laboratzen ez den lurra,
neguan bethi elhurra.

Han oihan, sasi handiak,
larreak eta mendiak,
harri, arroka gogorrak,
lur agor eta idorrak.

Han otsoak eta hartzak,
basoko bestia guziak,
alimalien herria,
desertu izigarria.

Ternuan dira salbaiak,
eta izkimau etsaiak,
giza bestia kruelak,
hilik jaten marinelak.

Barbarian bizitzea,
galeretan zahartzea,
ez da pena gehiago,
Ternuan ez gutiago.

Hango lanak eta penak,
akabatzen ez direnak,
lan bat egin duteneko,
mila baituzte hasteko.

Egunaz ez da pausurik,
ez gabaz errepausorik,
trabailuan bethi presa,
lana garai ezin sesa.

Ezin geldi, hori behar,
bethi egun, bethi bihar,
astelegun guzietan,
besta eta igandetan.

Gau-egunez lanez ase,
neurrimenduz bethi gose,
galdu jateko astia,
loa dute garastia.

Han ez da behar nagirik,
ez gezurrez den eririk,
han dakite eragiten,
alferra fetxo egiten.

Kamainan dago eria,
triste errain eroria,
nihork ez du urrikari,
zeren ez den lanean ari.

Uda luzean Ternua,
marinelen ifernua,
herrian parabisua,
bai azken errepausua.
