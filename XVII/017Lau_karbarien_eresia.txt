            Ora-gauan laur karbari
            karban hanxe ziren ari,
egarriz borx' egon-eta nehork ez hek urrikari.

            <<Nozbait,ala dioite elgarri,
            aho hoien hezagarri,
nonbait zerbait ezpadugu galdu behar gara sarri>>

            Hori erranik, hauk doazala
            hetarik biga, berhala,
baratzez baratze, xori ihiztariak bezala.

            Ezt' izan ehon hesirik
            hanbat goraki hersirik,
non eztituzten gaintitu, oillarrasiki jauzirik.

            Joanez, joanez, azkenean,
            unhe-hurran zirenean,
sagardoibaten barnera sar ziten, oren honean.

            Han pikotzebat baturik,
            frutaz oro kargaturik,
pot eta besarkaz batu zauzko, belhaunak gurturik.

            <<O zuhatz orsto-zabala
            (dioitela) luzez izala
horl' ukensu, horl' ekoizle, orai izana bezala;

            Zuhatzetan hi lehena
            aiz, hoben' et' ederrena ;
dohatsu hir' orsto-adarrak, dohatsu hir' ekoizpena.

            Etzen ez deitatzekoa,
            noizten, frutu bizizkoa
Eb' amasok jan zezana, bana bai, o hi pikoa.

            Zeren hartarik alhatu
            zelakoz, zen heriostatu
Eba, baita haren leinu oro minberaz kozatu;

            Aldiz, pikoa, higanik,
            hil-hurrenean izanik,
zitikeguk, guk biziak osagarri hik emanik>>.

            Hanbateki bata jauzten
            adarretar', eta hausten
ari d' aldaken, bertzea zuhatzaren iharrausten.

            Bild' uken duten bezala,
            biek jasan ezin-ahala,
herena, karbategiti hetara haur datorrala.

            Laurdenak ezpaitzakien,
            ber' egonik zer zaidien,
sorgin, et' ohoinen lotsaz, auzora laster zegien.

            Hango atea, barax, jorik,
            morroinbat han-berekorik
ager zekion, begitartez anderauren-gisakorik.

            Ustez zen anderaurena,
            ager zezon bere pena
bai et' eska hur, edo arno apurbaten ahamena.

            Hark, zer zen gauz' ikasirik,
            barnera sar erazirik,
eznez erregala zezan bere zurati erautsirik.

            Hanti-landan er' urzeki
            zezan, gogo handireki
karbategira, bait' egin karb' aldibat han hareki.

            Gero bertzen, urrundanik,
            jin-herotsa hautemanik,
berhez ziten, brist' elgarri pot eta besark' emanik:

            Arte hartan, hauk direla,
            jauziz jauzi, nol' igela,
bertze hirurak, alzoak pikoz bethe dakarztela.

            Laurdena, (punsu jarririk)
            herenak han ber' utzirik,
kexa zedin (alegia) egon zela galetsirik;

            Herenak zinhardetsana:
            eztinat nik ogen, bana
hihaurk, falta hirea dun eneki jin ez izana.

            Morroina beh' aurkintzetan
            baitzauten, aldarte hetan,
zer egin ziroen, oldar bat, egon zedin pensaketan.

            Ger' oharturik, gazt-ara
            Hizkatzerik eskukara
jin zaitezkiela, joan zedin, gordailuti hek beitara.

            Han jarririk ararteko
            Bilhakaiden baketzeko
igor zezan arno-bilha pikoekin ezkontzeko.

            Noizbait, bakeak eginik,
            Arno' ere orduko jinik,
uxtia zedin, pikoekin, garhaitu gab' utzikinik.
