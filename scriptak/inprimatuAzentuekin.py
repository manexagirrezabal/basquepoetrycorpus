
from lxml import etree
import sys
import re

#MODIFIED FROM http://stackoverflow.com/questions/287871/return-in-terminal-with-colors-using-python
def prRed(prt): return("\033[91m{}\033[00m" .format(prt))
def prGreen(prt): return("\033[92m{}\033[00m" .format(prt))
def prYellow(prt): return("\033[93m{}\033[00m" .format(prt))
def prLightPurple(prt): return("\033[94m{}\033[00m" .format(prt))
def prPurple(prt): return("\033[95m{}\033[00m" .format(prt))
def prCyan(prt): return("\033[96m{}\033[00m" .format(prt))
def prLightGray(prt): return("\033[97m{}\033[00m" .format(prt))
def prBlack(prt): return("\033[98m{}\033[00m" .format(prt))

#print prRed("Ti")+prGreen("ri")+prRed("ki")+prGreen(" tau")+prRed("ki")+prGreen("tau")

if len(sys.argv)>2 and sys.argv[2]=='MET':
  printMet=True
else:
  printMet=False

#parser = etree.XMLParser(encoding='utf-8')
poem=etree.parse(sys.argv[1], base_url=None).getroot()
if "{" in poem.tag: #from https://bitbucket.org/manexagirrezabal/poetrycorpusreader/
  namespace= re.sub("\}.*", "", poem.tag).replace("{","")
else:
  namespace=''
ns = {'ns':namespace}

title=poem.find("./ns:teiHeader/ns:fileDesc/ns:titleStmt/ns:title", ns).text.decode("utf8")
author=poem.find("./ns:teiHeader/ns:fileDesc/ns:titleStmt/ns:author", ns).text
year=poem.find("./ns:teiHeader/ns:fileDesc/ns:publicationStmt/ns:date", ns).text

#Centering
#https://www.safaribooksonline.com/library/view/python-cookbook-3rd/9781449357337/ch02s13.html

print prCyan(title).center(100).encode("utf8")
print prYellow(author).center(100).encode("utf8")
print prPurple(year).center(100).encode("utf8")
print 
print


# Now we just work with the first possible scansion.
#TODO If there are ambiguous scansions, print them too.
linegroups=poem.findall("./ns:text/ns:body/ns:lg", ns)
for linegroup in linegroups:
  lines=linegroup.findall("./ns:l",ns)
  for line in lines:
    segs=line.findall("./ns:seg",ns)
    pline=''
    syllind=0
    
    if printMet:
      allscansions=line.attrib['met']
    else:
      allscansions=line.attrib['real']
    scansion=allscansions.split("|")[0]
    scansions=allscansions.split("|")
    for seg in segs:
      if seg.attrib['type']=='syll':
        try:
          if scansion[syllind]=='+':
            pline=pline+prGreen(seg.text.encode("utf8"))
          else:
            pline=pline+prRed(seg.text.encode("utf8"))
        except IndexError:
          pline=pline+seg.text.encode("utf8")
        syllind+=1
      elif seg.attrib['type']=='space':
        pline=pline+' '
      elif seg.attrib['type']=='punct':
        pline=pline+seg.text.encode("utf8")
    for indeach,eachscansion in enumerate(scansions):
      if len(eachscansion) != syllind:
        print "K.O. ", indeach,
    print pline, allscansions
  print
  print
    
    
