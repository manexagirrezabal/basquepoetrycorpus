
from lxml import etree
import sys
import re



#print prRed("Ti")+prGreen("ri")+prRed("ki")+prGreen(" tau")+prRed("ki")+prGreen("tau")

#parser = etree.XMLParser(encoding='utf-8')
poem=etree.parse(sys.argv[1], base_url=None).getroot()
if "{" in poem.tag: #from https://bitbucket.org/manexagirrezabal/poetrycorpusreader/
  namespace= re.sub("\}.*", "", poem.tag).replace("{","")
else:
  namespace=''
ns = {'ns':namespace}

title=poem.find("./ns:teiHeader/ns:fileDesc/ns:titleStmt/ns:title", ns).text
author=poem.find("./ns:teiHeader/ns:fileDesc/ns:titleStmt/ns:author", ns).text
year=poem.find("./ns:teiHeader/ns:fileDesc/ns:publicationStmt/ns:date", ns).text

#Centering
#https://www.safaribooksonline.com/library/view/python-cookbook-3rd/9781449357337/ch02s13.html

print title
print author
print year
print 
print


# Now we just work with the first possible scansion.
#TODO If there are ambiguous scansions, print them too.
linegroups=poem.findall("./ns:text/ns:body/ns:lg", ns)
for linegroup in linegroups:
  lines=linegroup.findall("./ns:l",ns)
  for line in lines:
    segs=line.findall("./ns:seg",ns)
    pline=''
    syllind=0
    scansion=line.attrib['real'].split("|")[0]
    for seg in segs:
      if seg.attrib['type']=='syll':
        syllind+=1
        pline=pline+seg.text
      elif seg.attrib['type']=='space':
        pline=pline+' '
      elif seg.attrib['type']=='punct':
        pline=pline+seg.text

    print syllind, pline.encode("utf8")
  print
    
    
