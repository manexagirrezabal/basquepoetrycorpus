Ezagun du munduak
Jesus jaio dena;
ain dago mudatua,
ez dirudi lena;
len guzia zan pena,
negar, naibageak,
ta orain atsegin, gusto,
algara, farreak.

Gorroto, errierta,
auziak ta gerra,
gaitzak, gose, gezur ta
intentzio okerrak
juanik, ia ez dago
ezerren bildurrik,
ta ala dago mundua
zeru bat eginik.

Abogadu, eskribau
eta merioak,
mediku, barbero ta
botikarioak,
ez dira bear; ta argatik
izango da sarri
Hipokrates gaxoa
saguen janari.

Ezpataak, lantzaak eta
beste arma guziak
erositzen dituzte
errementariak;
ta alferrikan ez dedin
galdu burnia ura,
enpleatzera diaz
gure probetxura.

Atxur, burdinara ta
golde biurturik,
nekazari langinak
ongi giaturik,
emango digu lurrak
garia ta artoa,
gaztaina, sagardoa
ta ardoa naikoa.

Garia ta artoa
merke ta ugariro,
gaztaina ta sagarra
berriz urte-oro;
ta ardoak, galdurikan
lengo malizia,
ez gaitu moskortuko.
edan zia-zia.

Ez da onezkero esango
gezurrik batere,
egia esango dute
dendariak ere:
juan da emendik tranpa
ta fede gaistoa,
ia ur bage saltzen da
tabernan ardoa.

Basa arteko piztiaak
malso-malso eginik,
ez dute ya alkar jaten,
ta ain gitxi gizonik.
bat eginik guziak
alkarren artean
bizi dira etsai gabe,
kontentuz, bakean.

Otsoa eta ardia
zelai zabalean
sarri ekusten dira
biak jolasean;
alberdanian txita
aizariarekin.
portugesa laztanga
gastelaubarekin.

Auxen bai dala egiaz
denbora urrezkoa,
ta ez len koplari zarrak
erausitakoa;
orain aien ametsak
egia sorturik,
ekusten ari gera
pozez zoraturik.

Baina kanta berri au
geitxo luzatu da,
nere eztarri tristea
ia urratu da;
naiago det ixildu
katarratu baino;
Agur, nere jenteak,
urrengo urteraino.
