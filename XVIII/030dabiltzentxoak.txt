Erderaz eztezu nai
ene biotza,
euzkeraz kopla egiten
eztut nik lotza.
        Ikuzko deugu
        albadaigu egin zerbait
        nola bear degu.

Gaizki ba doaz ere
orra euzkeraz,
ifin naiz au egiten
neure bildurraz.
        Egizu farre
        gaizki mintzazen badut
        lotzarik bage.

Txoriak arboletan
dirade pozik,
euren libertadeaz
kantazeagatik.
        Alan nai nuke
        egon ni ere baina
        ezta posible.

Ai ene txori arinak
egatzen duzu,
jaulako karzelbaga
fortuna duzu.
        Nik ezin ala
        zerren loturik nago
        prisione baga.

Lenago ezaun du baino
uste duei guziak
Nerone bat zerala
bai zera eztiak.
        Dulze ta gozo
        zugatik gauza guztiak
        ixten dut oso.

Ene suertea da gogor
zu gogorrago
ezpazaizte bigundu
galdurik nago.
        Bigundu zaizte,
        orrekin gure gauzak
        dirade naste.

Farre egiten baduzu
dirudi aingeru
la ifin bazadiz seria
guzia da ilundu.
        Ene iguzkia,
        odei baga ikusko dut
        zure aurpegia.

Zure urrezko uleak
nauko galdurik,
katena orrekin nauko
ongi loturik.
        Ene maitea,
        askatu ta emadazu
        libertadea.

Ai, ene begi beltxak,
ilik naukazu,
euki egizu piedade
ta piztu naizu.
        Ai, ene mina,
        enetzat gauza guziak
        dirade ozpina.

Zure begietan dago
ene suertea,
nai baduzu bizia
edo eriotzea.
        Ene laztana,
        zugatik egingo dut
        nik al daidana.
