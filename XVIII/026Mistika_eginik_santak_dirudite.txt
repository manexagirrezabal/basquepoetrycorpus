A, zer umorea orai kantatzeko,
motiboa franko bertso paratzeko,
askok esaten dute alferren lana da
eta bertze zenbaitek ongi egina da,
inoren mingainen giltzarik eztut,
dena den bezala konfesatu nai dut.

Inork onelakorik eztu aditu,
nere jendeak Pasaian gertatu,
farra egiteko kontua da,
nere kristauak, bai ala da,
San Martinen etxean dabil duendea,
eztala besterik bere semea.

Jantzirik atera gauaz oni
soldadu trajean duende ori,
jendeak ala esaten elkarri
suia isilik ez al da etorri,
neroni ere artan nago,
piper eginik etxean dago.

Gauza klaroa da ezer eztela,
justifika beza duendea dela,
zer den duendea ote daki,
esan zaiozu ezpadaki,
guzia dabil gezurraz beterik,
etxearen errenta ezin pagaturik.

Asmatu det naski, señor San Martin,
zurea etxean duende au dabil,
orai artean ezta sentitu duenderik,
izatekoz etxean daduka gorderik,
jabea emaian etxea debalde,
ezta duenderik izango batere.

Probidentzia artu du orai justiziak,
manifestatu ditu onen maliziak,
bi gau entero etxean pasatu,
diote ezer ere eztela sentitu,
Pikaro San Martin, zer luke merezi,
grillete batekin zepoan iduki.

Bertze ofiziorik gizon onek eztu,
agoari beatza pasarazi nai dio,
baliatu zaio zenbait personakin,
baine gertatu da bere lagunekin,
juizio dutenak sinesten eztute,
zeren konsideratzen obeki bai dute.

Aditu bear nazu txokolateroa,
zure laguna da Pontoneroa,
gauaz juan eta egondu piska bat,
bigaramonean sentitu nuen bat,
ezagun da zerala gizon tontoa,
sinple, mentekato, bigarren boboa.

Irugarrena da zuen letradua,
ezertan ere ezta gizon agudoa,
duendea ba dela nai du defenditu,
onelako pastarik eztegu aditu,
asko da guzien endredatzeko
Egurza falta da sentenziatzeko.

Probintzian ezta abogadurik
auzian defendituko duenik,
juizio falta duen bat ezpada.
Guzien artean Kolejiala da,
Korrexidore banintz iru urteko
endredadore au kastigatzeko.

Nagusiaren kontra zertan eman dezu,
motiborik arkitzen ezpaldin badezu,
orai duendea aztu nai dezu,
ongi pensatu al baldin badezu
nik jakinen nuke zuri zer egin,
txakurrai eman fuera San Martin.

Zoaz keja onekin letraduagana.
kereila batekin atozke nigana,
gusto andiarekin zu zerbituko,
berdin erreparorik eztu paratuko,
parezera artzean kontu emazu,
nor izandu zaran justifika zatu.

Beste bat ere bada nai det explikatu
jakin zazue nor den, au da Julen Llulun,
sable erdoitua eskuan arturik,
duendeagana juan da sofokaturik,
sila bat artu eta salan jarririk,
tristea egondu zen ikaraturik.

Balore gutxikoa zara, señor Doctor,
digame VM si el duende tiene algun color,
uste zenduen nonbait oro bat juaitea
bisita egin ta pulsoa artzeko,
zer dela uste duzu gauazko kontua,
etxera etorri zinan guzia galdua.

Bigaramoneko zure feria,
gezurra kontatzen utzirik egia,
duendea bazela zuk aseguratzen,
astotxar guziaz au sinistatzen,
jende txoroagorik eztet ikusi,
itsuen artean okerra nagusi.

Ezer erran eztezan zugatik munduak
irakurri bear dezu Feijoren liburua,
ignorante guziak kontura erortzeko
eta sinple diranak desengainatzeko,
erremedioa arkituko dezu
orai presentean bearra badezu.

El medico, dice don Joseph Basanta
que por el duende VM se espanta,
ezpadu erregek soldadu oberik
aurreratuko eztu zurekin askorik,
argia itzalita onen kolerari
askailaretan nonbait beldurraz erori.

Itzegin nai det, nere kristauak,
anitz zarate konkista bearrak,
edozein sinpleza sorginkeria
sinistaturik dago jende guzia,
ezpaldin bazarete ongi konfesatzen
izango zerate errazak sinesten.

Konzepto onekoak etxuraz badire,
elizara juaiten ekusten dirade,
mistika eginik santak dirudite,
anitz kristautxo txoroak bai ala uste,
elkarren artean itz egitea,
pare gutxi dute murmuratzea.

Badire personak erri onetan
sinistatzen dutenak edozein gauzetan,
eztire kristauak onelakoak,
baizik dirudite ereje errikoak,
Labe bat nai nuke oientzat beroa
eta lenbizikoa San Martin ...
