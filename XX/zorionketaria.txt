Lehen lilian nuen bizia,
herritik joan nintzanean
Gazte-lagunen dantza pollitak
utzi nituen etxean...
Bai ondarea, bai ontasunak,
denak utzirik joaitean.
Alegeraki, giza-gaixoa,
esperantza bihotzean,
zorion-keta abiatu niz
bide-makhila eskuan.

Ustekeria berezi batek
baninderaman beilari
Mixteriozko dei batek behin
hitz hauk zaizkitan ekharri:
--<<Abil, maitea, Iguzki-alde,
jarraik hadi bideari.
Urhezko athe baterat-eta,
sar hadi, hel bezain sarri.
Hor lurra zeru bilhakatzen duk...
adin zorogaitzari! >>

Arratsa heldu, goiza ere jin,
sekulan ez niz gelditu.
Uhaitzek hautsi dautet bidea,
mendiak zaizkit etsaitu,
leize beltzenak, ur handienak
nolazpeit ditut garhaitu.
Udako bero, neguko hotzak,
denak ixilik pairatu...
bainan nik nahi nukeien hura
bethi bixtatik zait galdu.

Ibai eder bar aurkhitu nuen
Ekhi-alderat zoana.
Hartan fidatu, hori nuela
hel-biderik hoberena
--<<Othoi urhezko athe hartarat
ereman nezak, uhaina!>>
Uhain urdina, jauzian-jauzi,
badoa bethi aitzina...
Orai itsaso zabal erdian
nago harek eremana.

Eskuin bezala ezker so nago,
bi begiak xuriturik;
Ez dut ikhusten muga hartarat
ninderamakeen biderik.
Hastean bezain gibel niz eta
deus ez dezaket igurik.
Zerua naski goraxko dugu,
nehun ez hunkitzen lurrik...
Lur huntan nehoiz ez da sortuko
hango zorion osorik.



