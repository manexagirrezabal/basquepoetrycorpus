Bertzek erran baitute nik baino lehen
hainitz adituz dela hainitz ikhasten,
       uste dut on den
nik dakidan bezala guziek arren
       jakin dezaten...
Beraz aipha dezagun Maria nor den.

Aztal biribil eta zangar ertsia,
eskuaren beteko zango bihia
       ttiki ttikia,
golkoz aberats eta lerden gerria,
       erne begia...
Nolako panpina den horra Maria!

Erle bat da Maria Etxe-barnean
eta landan oro bat hari lanean
       behar denean.
aphaintzeko pulliki behin astean.
       -Noiz?... Igandean.
Uztai beharrik ez du soinaren pean.

Solas alegera bat erranagatik,
irri freskorik baizik ez dut izan nik
       Maria ganik.
Adiskidekin ez du handitasunik,
       bainan hargatik,
ez erran Mariari solas arinik.

Mariak baratzetik badu berea,
udan eta neguan lorez bethea
       errekartea.
bere loren artean erdi gordea,
       gaxo maitea!
Bera da pullitena hango lorea.

Mariak baldin badu moltsa joria:
merkatura goateko badu saskia,
       xuri-xuria.
Arroltze lau dotzena, oilasko bia,
       salduz guzia,
zertako den aberats horra Maria!

Jendek erranik ere hala eta hola...
Maria batzuetan, ez jakin nola,
       goibel dagola...
Bertzetan nigartto bat jausten zaiola...
       Zer zaio axola?
Baluke nahi balu aise konsola.

Igandetan Maria zinez panpina,
zapata xabalekin soinmotx urdina,
       berak egina,
elizako bidean arin-arina;
       mila sorgina!
Errege baldin banitz zer erregina!
