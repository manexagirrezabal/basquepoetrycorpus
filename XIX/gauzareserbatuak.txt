Dozena bat zortziko Marijoan Igoak
nere bizimodua jakin dezan munduak
ondo esplikatzea eman dit gogoak
laguntzen baldin badit gure Jaungoikoak.

Apez bat zerbitu dut amalau urtean,
jaun eta jabe eginik aren ondasunean,
Señora bat bezala jauntzirik soinean,
pagu ona eman dit orai atzenean.

Onela esan zidan urtea bete gabe:
--, <<Zure faltan nago ni osasuna gabe>>.
--<<Atoz nere oiara pasatzera gaube>>.
Pozik agindu nion erreparo gabe.

Sazerdote batean ez dira kabitzen
burutikan galdua ezpada arkitzen;
jarritzen ginanean graziak ematen
gauza reserbatuak zizkidan esaten.

Joan den abendoan eguerri jaietan,
nere lagunarekin endemas gaubetan,
gustora egon nintzan loka arrotzetan.

Bederatzi ilabete urbilen urrian
justo kunplitu ziran ogei ta bostean,
oinaze ta dolore aundien artean
xita bat atera zen ez ustekabean.

Dama gazte batentzat alako otsoa
aisa errenditu nau pikaro faltsoa,
seme bat eragin dit bere antzekoa
duda gabe eztuke lendabizikoa.

Ojala baldin banintz munduan bakarrik,
onela bizitu dana disimulaturik.
Lain egina urriki ezta probetxurik,
ni emen gelditzen naiz dena urraturik.

Doktore medikuek etzioten igarri
zer pasatzen zitzaion nere sabelari,
ordenatu zioten mila bat edari
eskerrak sor dizkiot txikirotelari.

Bizimodu polita dago neretako,
apeza ez dezaket artu senartako,
norbaitek nai banindu bere andretako
makila bizkarrean etzaide faltako.

Adios, Erizeko zarrak eta gazteak,
nik iduki zaituzket nasirik guztiak,
ala agintzen zidan nere nagusiak,
orain agertu dira gure maliziak.

Salve, Regina Mater misericordia,
anparatu nazazu, Birjina Maria,
ongo konfesatzeko indazu grazia,
nere anima tristeak badauka premia.
